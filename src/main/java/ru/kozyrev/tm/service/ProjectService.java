package ru.kozyrev.tm.service;

import ru.kozyrev.tm.entity.Project;
import ru.kozyrev.tm.repository.ProjectRepository;
import ru.kozyrev.tm.util.DateUtil;

import java.text.ParseException;
import java.util.List;

public class ProjectService {
    ProjectRepository projectRepository;

    public ProjectService(ProjectRepository projectRepository) {
        this.projectRepository = projectRepository;
    }

    public Project findOne(String id) {
        return projectRepository.findOne(id);
    }

    public List<Project> findAll() {
        return projectRepository.findAll();
    }

    public void persist(String name, String description, String dateStart, String dateFinish) throws ParseException {
        Project project;
        //if we have collision
        while (projectRepository.findOne((project = new Project()).getId()) != null) {
        }
        updateProject(project, name, description, dateStart, dateFinish);
        projectRepository.persist(project);
    }

    public void merge(String num, String name, String description, String dateStart, String dateFinish) throws ParseException {
        String projectId = getIdByNum(num);
        if (projectId == null) {
            persist(name, description, dateStart, dateFinish);
        } else {
            Project project = findOne(projectId);
            updateProject(project, name, description, dateStart, dateFinish);
            projectRepository.merge(project);
        }
    }

    public Project remove(String num) throws RuntimeException {
        return projectRepository.remove(getIdByNum(num));
    }

    public void removeAll() {
        projectRepository.removeAll();
    }

    private void updateProject(Project project, String name, String description, String dateStart, String dateFinish) throws ParseException {
        if (name.length() != 0 && name != null) {
            project.setName(name);
        }
        if (description.length() != 0 && description != null) {
            project.setDescription(description);
        }
        if (dateStart.length() != 0 && dateStart != null) {
            project.setDateStart(DateUtil.parseDate(dateStart));
        }
        if (dateFinish.length() != 0 && dateFinish != null) {
            project.setDateFinish(DateUtil.parseDate(dateFinish));
        }
    }

    private String getIdByNum(String num) {
        if (num.length() == 0) {
            return null;
        }
        List<Project> list = findAll();
        int index = Integer.parseInt(num) - 1;
        if (index < 0 || index > list.size() - 1) {
            return null;
        }
        return list.get(index).getId();
    }
}
