package ru.kozyrev.tm.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DateUtil {
    private static SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");

    public static String getDate(Date date) {
        return formatter.format(date);
    }

    public static Date parseDate(String date) throws ParseException {
        return formatter.parse(date);
    }
}
