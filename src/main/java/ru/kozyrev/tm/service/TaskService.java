package ru.kozyrev.tm.service;

import ru.kozyrev.tm.entity.Project;
import ru.kozyrev.tm.entity.Task;
import ru.kozyrev.tm.repository.ProjectRepository;
import ru.kozyrev.tm.repository.TaskRepository;
import ru.kozyrev.tm.util.DateUtil;

import java.text.ParseException;
import java.util.Iterator;
import java.util.List;

public class TaskService {
    TaskRepository taskRepository;
    ProjectRepository projectRepository;

    public TaskService(TaskRepository taskRepository, ProjectRepository projectRepository) {
        this.taskRepository = taskRepository;
        this.projectRepository = projectRepository;
    }

    public Task findOne(String id) {
        return taskRepository.findOne(id);
    }

    public List<Task> findAll() {
        return taskRepository.findAll();
    }

    public void persist(String name, String projectNum, String description, String dateStart, String dateFinish) throws ParseException {
        Task task;
        //if we have collision
        while (projectRepository.findOne((task = new Task()).getId()) != null) {
        }
        updateTask(task, name, projectNum, description, dateStart, dateFinish);
        taskRepository.persist(task);
    }

    public void merge(String taskNum, String name, String projectNum, String description, String dateStart, String dateFinish) throws ParseException {
        String taskId = getTaskIdByNum(taskNum);
        if (taskId == null) {
            persist(name, projectNum, description, dateStart, dateFinish);
        } else {
            Task task = findOne(taskId);
            updateTask(task, name, projectNum, description, dateStart, dateFinish);
            taskRepository.merge(task);
        }
    }

    public Task remove(String taskNum) {
        return taskRepository.remove(getTaskIdByNum(taskNum));
    }

    public Boolean removeProjectTasks(String projectNum) {
        Boolean isDelete = false;
        String projectId = getProjectIdByNum(projectNum);
        List<Task> list = findAll();
        Iterator<Task> iterator = list.iterator();
        while (iterator.hasNext()) {
            Task task = iterator.next();
            if (task.getProjectId().equals(projectId)) {
                taskRepository.remove(task.getId());
                isDelete = true;
            }
        }
        return isDelete;
    }

    public void removeAll() {
        taskRepository.removeAll();
    }

    private void updateTask(Task task, String name, String projectNum, String description, String dateStart, String dateFinish) throws ParseException {
        if (name.length() != 0 && name != null) {
            task.setName(name);
        }

        String projectId = getProjectIdByNum(projectNum);
        if (projectId != null) {
            task.setProjectId(projectId);
        }
        if (description.length() != 0 && description != null) {
            task.setDescription(description);
        }
        if (dateStart.length() != 0 && dateStart != null) {
            task.setDateStart(DateUtil.parseDate(dateStart));
        }
        if (dateFinish.length() != 0 && dateFinish != null) {
            task.setDateFinish(DateUtil.parseDate(dateFinish));
        }
    }

    public String getTaskIdByNum(String num) {
        if (num.length() == 0) {
            return null;
        }
        List<Task> list = findAll();
        int index = Integer.parseInt(num) - 1;
        if (index < 0 || index > list.size() - 1) {
            return null;
        }
        return list.get(index).getId();
    }

    public String getProjectIdByNum(String num) {
        if (num.length() == 0) {
            return null;
        }
        List<Project> list = projectRepository.findAll();
        int index = Integer.parseInt(num) - 1;
        if (index < 0 || index > list.size() - 1) {
            return null;
        }
        return list.get(index).getId();
    }
}
