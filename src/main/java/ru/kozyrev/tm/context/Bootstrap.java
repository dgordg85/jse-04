package ru.kozyrev.tm.context;

import ru.kozyrev.tm.enumerated.TerminalCommand;
import ru.kozyrev.tm.repository.ProjectRepository;
import ru.kozyrev.tm.repository.TaskRepository;
import ru.kozyrev.tm.service.ProjectService;
import ru.kozyrev.tm.service.TaskService;
import ru.kozyrev.tm.view.ProjectView;
import ru.kozyrev.tm.view.TaskView;

import java.text.ParseException;
import java.util.Scanner;

public class Bootstrap {
    private ProjectRepository projectRepository;
    private ProjectService projectService;
    private ProjectView projectView;
    private TaskRepository taskRepository;
    private TaskService taskService;
    private TaskView taskView;
    private Scanner sc = new Scanner(System.in);

    public Bootstrap() {
        projectRepository = new ProjectRepository();
        taskRepository = new TaskRepository();
        projectService = new ProjectService(projectRepository);
        taskService = new TaskService(taskRepository, projectRepository);
        taskView = new TaskView(taskService, sc);
        projectView = new ProjectView(projectService, taskView, sc);

        try {
            projectService.persist("Проект 1", "Описание", "10-12-2020", "01-01-2021");
            projectService.persist("Проект 2", "Описание", "10-12-2021", "01-01-2021");
            projectService.persist("Проект 3", "Описание", "10-12-2021", "01-01-2021");
            projectService.persist("Проект 4", "Описание", "10-12-2021", "01-01-2021");
            projectService.persist("Проект 5", "Описание", "12-12-2020", "01-01-2021");
            projectService.persist("Проект 6", "Описание", "10-12-2021", "01-01-2021");
            taskService.persist("Задача 1", "1", "Описание задачи", "01-20-2017", "01-20-2017");
            taskService.persist("Задача 2", "1", "Описание задачи", "01-20-2018", "01-20-2017");
            taskService.persist("Задача 3", "1", "Описание задачи", "01-15-2017", "01-20-2017");
            taskService.persist("Задача 1", "2", "Описание задачи", "01-20-2017", "01-20-2017");
            taskService.persist("Задача 2", "2", "Описание задачи", "01-20-2017", "01-20-2017");
            taskService.persist("Задача 3", "2", "Описание задачи", "01-14-2017", "01-20-2017");
            taskService.persist("Задача 4", "2", "Описание задачи", "01-20-2017", "01-20-2017");
            taskService.persist("Задача 5", "2", "Описание задачи", "01-20-2017", "01-20-2017");
            taskService.persist("Задача 1", "3", "Описание задачи", "01-20-2017", "01-20-2017");
            taskService.persist("Задача 2", "3", "Описание задачи", "01-20-2017", "01-20-2017");
            taskService.persist("Задача 1", "4", "Описание задачи", "01-20-2017", "01-20-2017");
            taskService.persist("Задача 2", "4", "Описание задачи", "01-20-2017", "01-20-2017");
            taskService.persist("Задача 1", "5", "Описание задачи", "01-20-2017", "01-20-2017");
            taskService.persist("Задача 1", "6", "Описание задачи", "01-20-2017", "01-20-2017");
            taskService.persist("Задача 2", "6", "Описание задачи", "01-20-2017", "01-20-2017");
            taskService.persist("Задача 3", "6", "Описание задачи", "01-20-2017", "01-20-2017");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void init() {
        TerminalCommand command;
        System.out.println("*** WELCOME TO TASK MANAGER ***");
        while (true) {
            try {
                command = TerminalCommand.valueOf(sc.nextLine().toUpperCase().replace('-', '_'));
                if (command.equals(TerminalCommand.EXIT)) {
                    break;
                }
                make(command);
            } catch (NumberFormatException | IndexOutOfBoundsException e) {
                System.out.println("Wrong index!");
            } catch (IllegalArgumentException e) {
                System.out.println("Wrong command! Use 'help'!");
            } catch (ParseException e) {
                System.out.println("Wrong date! Use dd-MM-YYYY!");
            }
        }
        sc.close();
    }

    public void make(TerminalCommand command) throws ParseException {
        switch (command) {
            case HELP:
                TerminalCommand.print();
                break;
            case PROJECT_CLEAR:
                projectView.clear();
                break;
            case PROJECT_UPDATE:
                projectView.update();
                break;
            case PROJECT_CREATE:
                projectView.create();
                break;
            case PROJECT_LIST:
                projectView.getList();
                break;
            case PROJECT_REMOVE:
                projectView.remove();
                break;
            case TASK_CLEAR:
                taskView.clear();
                break;
            case TASK_UPDATE:
                taskView.update();
                break;
            case TASK_CREATE:
                taskView.create();
                break;
            case TASK_LIST:
                taskView.getList();
                break;
            case TASK_REMOVE:
                taskView.remove();
                break;
        }
    }
}
