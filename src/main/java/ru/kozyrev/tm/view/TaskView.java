package ru.kozyrev.tm.view;

import ru.kozyrev.tm.entity.Task;
import ru.kozyrev.tm.service.TaskService;

import java.text.ParseException;
import java.util.*;

public class TaskView {
    private TaskService taskService;
    private Scanner sc;

    public TaskView(TaskService taskService, Scanner sc) {
        this.taskService = taskService;
        this.sc = sc;
    }

    public void create() throws ParseException {
        System.out.println("[TASK CREATE]\nENTER NAME:");
        String name = sc.nextLine();
        System.out.println("ENTER PROJECT ID:");
        String projectNum = sc.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        String description = sc.nextLine();
        System.out.println("ENTER DATESTART:");
        String dateStart = sc.nextLine();
        System.out.println("ENTER DATEFINISH:");
        String dateFinish = sc.nextLine();
        taskService.persist(name, projectNum, description, dateStart, dateFinish);
        System.out.println("[OK]");
        printList(projectNum);
    }

    public void update() throws ParseException {
        System.out.println("[UPDATE TASK]\nENTER ID:");
        String taskNum = sc.nextLine();
        System.out.println("NEW NAME:");
        String name = sc.nextLine();
        System.out.println("NEW PROJECT ID:");
        String projectNum = sc.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        String description = sc.nextLine();
        System.out.println("ENTER DATESTART:");
        String dateStart = sc.nextLine();
        System.out.println("ENTER DATEFINISH:");
        String dateFinish = sc.nextLine();
        taskService.merge(taskNum, name, projectNum, description, dateStart, dateFinish);
        System.out.println("[OK]");
        printList();
    }

    public void getList() {
        System.out.println("PRESS 'ENTER' FOR ALL OR INPUT ID PROJECT");
        String projectNum = sc.nextLine();
        if (projectNum.length() == 0) {
            printList();
        } else {
            printList(projectNum);
        }
    }

    public void printList() {
        System.out.println("[TASKS LIST]");
        List<Task> taskList = taskService.findAll();
        for (int i = 0; i < taskList.size(); i++) {
            System.out.printf("%d. %s, PROJECT ID# %s, TASK ID# %s\n", i + 1, taskList.get(i).getName(), taskList.get(i).getProjectId(), taskList.get(i).getId());
        }
    }

    public void printList(String projectNum) {
        String projectId = taskService.getProjectIdByNum(projectNum);
        System.out.println("[TASKS LIST OF PROJECT]");
        List<Task> taskList = taskService.findAll();
        int count = 1;
        for (int i = 0; i < taskList.size(); i++) {
            if (taskList.get(i).getProjectId().equals(projectId)) {
                System.out.printf("%d. %s, EDIT ID# %s, PROJECT ID# %s\n", count++, taskList.get(i).getName(), i, taskList.get(i).getProjectId());
            }
        }
    }

    public void remove() {
        System.out.println("[TASK DELETE]\nENTER ID:");
        String taskNum = sc.nextLine();
        if (taskService.remove(taskNum) != null) {
            System.out.println("[OK]");
        } else {
            throw new IndexOutOfBoundsException();
        }
    }

    public void clear() {
        System.out.println("[CLEAR]");
        System.out.println("Press 'ENTER' FOR ALL OR PROJECT ID]");
        String projectNum = sc.nextLine();
        if (projectNum.length() == 0) {
            clearAll();
        } else {
            clear(projectNum);
        }
    }

    public void clear(String projectNum) {
        if (taskService.removeProjectTasks(projectNum)) {
            System.out.println("[ALL TASKS OF PROJECT REMOVED]");
        }
    }

    public void clearAll() {
        taskService.removeAll();
        System.out.println("[ALL TASKS REMOVED]");
    }
}
