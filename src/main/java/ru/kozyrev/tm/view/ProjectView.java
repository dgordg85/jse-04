package ru.kozyrev.tm.view;

import ru.kozyrev.tm.entity.Project;
import ru.kozyrev.tm.service.ProjectService;

import java.text.ParseException;
import java.util.List;
import java.util.Scanner;

public class ProjectView {
    private ProjectService projectService;
    private TaskView taskView;
    private Scanner sc;

    public ProjectView(ProjectService projectService, TaskView taskView, Scanner sc) {
        this.projectService = projectService;
        this.taskView = taskView;
        this.sc = sc;
    }

    public void create() throws ParseException {
        System.out.println("[PROJECT CREATE]\nENTER NAME:");
        String name = sc.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        String description = sc.nextLine();
        System.out.println("ENTER DATESTART:");
        String dateStart = sc.nextLine();
        System.out.println("ENTER DATEFINISH:");
        String dateFinish = sc.nextLine();
        projectService.persist(name, description, dateStart, dateFinish);
        printList();
    }

    public void remove() {
        System.out.println("[PROJECT REMOVE]\nENTER ID:");
        String projectNum = sc.nextLine();
        clear(projectNum);
    }

    public void update() throws ParseException {
        System.out.println("[UPDATE PROJECT]\nENTER ID:");
        String projectNum = sc.nextLine();
        System.out.println("ENTER NAME:");
        String name = sc.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        String description = sc.nextLine();
        System.out.println("ENTER DATESTART:");
        String dateStart = sc.nextLine();
        System.out.println("ENTER DATEFINISH:");
        String dateFinish = sc.nextLine();
        projectService.merge(projectNum, name, description, dateStart, dateFinish);
        System.out.println("[OK]");
    }

    public void getList() {
        System.out.println("Press 'ENTER' for printing all projects list or input 'ID PROJECT'");
        String projectNum = sc.nextLine();
        if (projectNum.length() == 0) {
            printList();
        } else {
            printList(projectNum);
        }
    }

    public void printList() {
        System.out.println("[PROJECTS LIST]");
        List<Project> projects = projectService.findAll();
        for (int i = 0; i < projects.size(); i++) {
            System.out.printf("%d. %s, ID# %s\n", i + 1, projects.get(i).getName(), projects.get(i).getId());
        }
    }

    public void printList(String projectNum) {
        Project project = projectService.findOne(projectNum);
        System.out.printf("[PROJECT '%s']\n", project.getName());
        taskView.printList(projectNum);
    }

    public void clear() {
        taskView.clearAll();
        projectService.removeAll();
        System.out.println("[ALL PROJECTS REMOVED]");
    }

    public void clear(String projectNum) {
        taskView.clear(projectNum);
        Project project = projectService.remove(projectNum);
        if (project != null) {
            System.out.println("[PROJECT REMOVED]");
        } else {
            throw new IndexOutOfBoundsException();
        }
        printList();
    }
}
